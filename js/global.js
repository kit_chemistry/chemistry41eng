//JQUERY SELECTORS
var derButton, factsButton, labButton, links,
	menuFrame, frames, model, modelContent;

var audio = [],
	audioPieces = [];
	
//SOUNDS
var buttonSound, fireworkSound, speechSound;

//SPRITES
var injectorSprite; 

//OTHER GLOBAL DATA
var shots, currentShot, 
	globalName;

function AudioPiece(source, start, end)
{
	this.audio = new Audio("audio/" + source + ".mp3");
	this.start = start;
	this.end = end;
	
	var currPiece = this,
		currAudio = this.audio;
	
	currAudio.addEventListener("timeupdate", function(){
		var currTime = Math.round(currAudio.currentTime);
		
		if(currTime == currPiece.end)
			currAudio.pause();
	});
}

AudioPiece.prototype.play = function()
{
	this.audio.currentTime = this.start;
	this.audio.play();
}

AudioPiece.prototype.pause = function()
{
	this.audio.pause();
}

AudioPiece.prototype.addEventListener = function(e, callBack)
{
	var currPiece = this,
		currAudio = this.audio,
		currDuration = Math.round(currAudio.duration);
	
	if(e === "ended")
	{
		console.log("else end: " + currPiece.end + ", " + currDuration);
		currAudio.addEventListener("timeupdate", function(){
			var currTime = Math.round(currAudio.currentTime);
				
			if(currTime === currPiece.end)
			{
				currAudio.pause();
				currAudio.currentTime += 3;
				callBack();
			}
		});
	}
	if(e === "timeupdate")
	{
		currPiece.audio.addEventListener("timeupdate", callBack);
	}
}

AudioPiece.prototype.getStart = function()
{
	return this.start;
}

var setMenuStuff = function(){
	var enterButton = $(".enter-button"),
		annotation = $("#frame-000 .annotation-hidden"),
		annotationButton = $("#frame-000 .annotation-button"),
		password = $(".password"),
		error = $(".error"),
		name = $(".name");
		name.val("");
		password.val("");
		
		
		sayHello = function(){
		regBox.html("Здравствуйте, " + name.val() + "!");
		regBox.css("height", "3em");
		regBox.css("padding", "0.5em");
		regBox.css("text-align", "center");
		globalName = name.val();
		regBox.addClass("topcorner");
		timeout[0] = setTimeout(function(){
			regBox.html(name.val());
			regBox.css("width", name.val().length + "em");
		}, 3000);
	};
	
	if(globalName)
		sayHello();
	
	var annotationButtonListener = function(){
		annotation.toggleClass("annotation-shown");
		annotationButton.toggleClass("annotation-button-close");

	};
	annotationButton.off("click", annotationButtonListener);
	annotationButton.on("click", annotationButtonListener);
	
	var enterButtonListener = function(){
		if(password.val() === "12345")
			sayHello();
		else
			error.html("неверный пароль");
	};
	enterButton.off("click", enterButtonListener);
	enterButton.on("click", enterButtonListener);
}

var plusOneSec = function(audiofile)
{
	var currTime = Math.round(audiofile.currentTime);
	audiofile.currentTime = ++currTime;
}

var setLRSData = function(){
	tincan = new TinCan (
    {
        recordStores: [
            {
                "endpoint": "http://54.154.57.220/data/xAPI/",
				"username": "e083498f4e256e68ab2c5ae2be4195d9a348eb20",
				"password": "c6ea3c32a9d77919d0eb3cdea3bc5d460f50d93b"
            }
        ]
    });
}

var sendLaunchedStatement = function(sm)
{
	if(globalName)
	{
		tincan.sendStatement(
		{
			"actor": {
				"objectType": "Agent",
				"account": {
					"name": globalName,
					"homePage": "http://lcms.nis.kz"
				}
			},
			"verb": {
				"id": "http://adlnet.gov/expapi/verbs/launched"
			},
			"context": {
				"platform": "web"
			},
			"object": {
				"id": "http://lcms.nis.kz/activity/821eb006-58bb-4154-b732-4b2a9a9330ad",
				"objectType": "Activity",
				"description": "frame-" + sm
			}
		});
	}
}

var sendCompletedStatement = function(sm)
{
	if(globalName)
	{
		tincan.sendStatement(
		{
			"actor": {
				"objectType": "Agent",
				"account": {
					"name": globalName,
					"homePage": "http://lcms.nis.kz/chemistry"
				}
			},
			"verb": {
				"id": "http://adlnet.gov/expapi/verbs/completed"
			},
			"context": {
				"platform": "web"
			},
			"object": {
				"id": "http://lcms.nis.kz/activity/821eb006-58bb-4154-b732-4b2a9a9330ad",
				"objectType": "Activity",
				"description": "frame-" + sm
			}
		});
	}
}


//TIMEOUTS
var timeout = [],
	sprite = [];
var launch = [];

launch["frame-000"] = function()
{
	theFrame = $("#frame-101"),
	theClone = theFrame.clone();
	fadeNavsOut();
}

launch["frame-101"] = function()
{		
		theFrame = $("#frame-101"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ", 
		girlMouth = $(prefix + ".girl-mouth");
		
	audioPieces[0] = new AudioPiece("s2-1", 0, 5);
		
	girlMouth.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	audioPieces[0].addEventListener("ended", function(){
		girlMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			hideEverythingBut($("#frame-102"));
			sendCompletedStatement(1);
		}, 2000);
	});
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			audioPieces[0].play();
			girlMouth.fadeIn(0);
		}, 2000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(1);
	}, 2000);
}

launch["frame-102"] = function()
{		
		theFrame = $("#frame-102"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boyMouth = $(prefix + ".boy-mouth");
	
	boyMouth.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	audioPieces[0] = new AudioPiece("s2-1", 5, 16);
	audioPieces[0].addEventListener("ended", function(){
		boyMouth.fadeOut(0);
		timeout[1] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			hideEverythingBut($("#frame-103"));
			sendCompletedStatement(1);
		}, 1000);
	});
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			boyMouth.fadeIn(0);
			audioPieces[0].play();
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(2);
	}, 2000);
}

launch["frame-103"] = function()
{		
		theFrame = $("#frame-103"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boyMouth = $(prefix + ".boy-mouth");
		
	audioPieces[0] = new AudioPiece("s2-1", 17, 21);
		
	boyMouth.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	audioPieces[0].addEventListener("ended", function(){
		boyMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			hideEverythingBut($("#frame-104"));
			sendCompletedStatement(1);
		}, 2000);
	});
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			boyMouth.fadeIn(0);
			audioPieces[0].play();
		}, 1000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(3);
	}, 2000);
}

launch["frame-104"] = function()
{		
		theFrame = $("#frame-104"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ", 
		boyMouth = $(prefix + ".boy-mouth"), 
		girlMouth = $(prefix + ".girl-mouth"),
		girlMouthIdle = $(prefix + ".girl-mouth-idle"),
		vinegar = $(prefix + ".vinegar");
		
	audioPieces[0] = new AudioPiece("s2-1", 21, 32);
		
	boyMouth.fadeOut(0);
	girlMouth.fadeOut(0);
	vinegar.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	audioPieces[0].addEventListener("ended", function(){
		girlMouth.fadeOut(0);
		girlMouthIdle.fadeIn(0);
		vinegar.fadeOut(1000);
		timeout[0] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}, 2000);
	});
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			girlMouthIdle.fadeOut(0);
			girlMouth.fadeIn(0);
			audioPieces[0].play();
		}, 1000);
		timeout[1] = setTimeout(function(){
			vinegar.fadeIn(500);
		}, 3000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(4);
	}, 2000);
}

launch["frame-105"] = function()
{		
		theFrame = $("#frame-105"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		girlMouth = $(prefix + ".girl-mouth"),
		girlMouthIdle = $(prefix + ".girl-mouth-idle"),
		demoPics = $(prefix + ".demo-pic"), 
		lemon = $(prefix + ".lemon"), 
		cola = $(prefix + ".cola"), 
		orange = $(prefix + ".orange");
	
	demoPics.fadeOut(0);
	girlMouth.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	audio[0] = new Audio("audio/s2-1.mp3");
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			audio[0].currentTime = 32;
			audio[0].play();
			girlMouthIdle.fadeOut(0);
			girlMouth.fadeIn(0);
		}, 1000);
		timeout[1] = setTimeout(function(){
			cola.fadeIn(500);
		}, 3000);
		timeout[2] = setTimeout(function(){
			cola.fadeOut(500);
			orange.fadeIn(500);
		}, 5000);
		timeout[3] = setTimeout(function(){
			orange.fadeOut(500);
			lemon.fadeIn(500);
		}, 8000);
		timeout[4] = setTimeout(function(){
			girlMouth.fadeOut(0);
			girlMouthIdle.fadeIn(0);
			lemon.fadeOut(500);
			timeout[0] = setTimeout(function(){
				theFrame.attr("data-done", "true");
				fadeNavsInAuto();
				sendCompletedStatement(1);
			}, 2000);
		}, 19000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(5);
	}, 2000);
}

launch["frame-106"] = function()
{		
		theFrame = $("#frame-106"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		girlMouth = $(prefix + ".girl-mouth"),
		girlMouthIdle = $(prefix + ".girl-mouth-idle"),
		demoPics = $(prefix + ".demo-pic"),
		ant = $(prefix + ".ant"), 
		acids = $(prefix + ".acids"),
		burntHand = $(prefix + ".burnt-hand"),
		handWash = $(prefix + ".hand-wash"),
		caustic = $(prefix + ".caustic"), 
		cross = $(prefix + ".cross");
	
	sprite[0] = new Motio(acids[0], {
		"fps": "2", 
		"frames": 5
	});
	sprite[0].on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	sprite[1] = new Motio(handWash[0], {
		"fps": "2", 
		"frames": 12
	});
	sprite[1].on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	demoPics.fadeOut(0);
	girlMouth.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	audio[0] = new Audio("audio/s3-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		girlMouth.fadeOut(0);
		girlMouthIdle.fadeIn(0);
		handWash.fadeOut(500);
		timeout[0] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			hideEverythingBut($("#frame-107"));
			sendCompletedStatement(1);
		}, 2000);
	});
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			audio[0].play();
			girlMouthIdle.fadeOut(0);
			girlMouth.fadeIn(0);
		}, 1000);
		timeout[1] = setTimeout(function(){
			ant.fadeIn(500);
		}, 6000);
		timeout[2] = setTimeout(function(){
			ant.fadeOut(500);
			acids.fadeIn(500);
		}, 9000);
		timeout[3] = setTimeout(function(){
			sprite[0].play();
		}, 11000);
		timeout[4] = setTimeout(function(){
			acids.fadeOut(0);
			caustic.fadeIn(0);
		}, 18000);
		timeout[5] = setTimeout(function(){
			caustic.fadeOut(0);
			cross.fadeIn(0);
		}, 20000);
		timeout[6] = setTimeout(function(){
			cross.fadeOut(0);
			burntHand.fadeIn(500);
		}, 25000);
		timeout[7] = setTimeout(function(){
			burntHand.fadeOut(500);
			handWash.fadeIn(500);
		}, 29000);
		timeout[7] = setTimeout(function(){
			sprite[1].play();
		}, 30000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(6);
	}, 2000);
}

launch["frame-107"] = function()
{		
		theFrame = $("#frame-107"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		girlMouth = $(prefix + ".girl-mouth"),
		girlMouthIdle = $(prefix + ".girl-mouth-idle"),
		acidPlusMetal = $(prefix + ".acid-plus-metal"), 
		acidPlusMagnesium = $(prefix + ".acid-plus-magnesium"),
		videoContainer = $(prefix + ".video-container"),
		video = $(prefix + ".video");
	
	video.attr("width", video.parent().css("width"));
	video.attr("height", video.parent().css("height"));
	
	$(window).resize(function(){
		video.attr("width", video.parent().css("width"));
		video.attr("height", video.parent().css("height"));
	});
	
	girlMouth.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	video.fadeOut(0);
	acidPlusMetal.fadeOut(0);
	acidPlusMagnesium.fadeOut(0);
	
	audioPieces[0] = new AudioPiece("s4-1", 0, 30);
	
	imgAcidPlusMetal = new Image();
	imgAcidPlusMetal.src = "pics/s7-acid-plus-metal.gif";
	
	imgAcidPlusMagnesium = new Image();
	imgAcidPlusMagnesium.src = "pics/s7-acid-plus-magnesium.gif";
	
	imgGirlMouth = new Image();
	imgGirlMouth.src = "pics/../pics/s4-girl-mouth.gif";
	
	video[0].addEventListener("ended", function(){
		girlMouth.fadeOut(0);
		girlMouthIdle.fadeIn(0);
		video.fadeOut(500);
		
		timeout[0] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}, 2000);
	});
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			audioPieces[0].play();
			girlMouthIdle.fadeOut(0);
			girlMouth.fadeIn(0);
		}, 1000);
		timeout[1] = setTimeout(function(){
			acidPlusMetal.fadeIn(500);
		}, 8000);
		timeout[2] = setTimeout(function(){
			acidPlusMetal.css("background-image", "url(pics/s7-acid-plus-metal.gif)");
		}, 10000);
		timeout[3] = setTimeout(function(){
			acidPlusMetal.fadeOut(500);
			acidPlusMagnesium.fadeIn(500);
		}, 16000);
		timeout[4] = setTimeout(function(){
			acidPlusMagnesium.css("background-image", "url(pics/s7-acid-plus-magnesium.gif)");
		}, 18000);
		timeout[5] = setTimeout(function(){
			acidPlusMagnesium.fadeOut(500);
			video.fadeIn(0);
			video[0].play();
		}, 23000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(7);
	}, 2000);
}

launch["frame-108"] = function()
{		
		theFrame = $("#frame-108"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		girlMouth = $(prefix + ".girl-mouth"),
		girlMouthIdle = $(prefix + ".girl-mouth-idle"),
		boyMouth = $(prefix + ".boy-mouth"),
		demoPics = $(prefix + ".demo-pic"), 
		activeMetalsPlusAcid = $(prefix + ".active-metals-plus-acid"), 
		inactiveMetalsPlusAcid = $(prefix + ".inactive-metals-plus-acid");
	
	sprite[0] = new Motio(activeMetalsPlusAcid[0], {
		"fps": "1", 
		"frames": 9
	});
	sprite[0].on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	sprite[1] = new Motio(inactiveMetalsPlusAcid[0], {
		"fps": "1", 
		"frames": 8
	});
	sprite[1].on("frame", function(){
		if(this.frame === this.frames - 1)
		{
			this.pause();
			
			timeout[0] = setTimeout(function(){
				girlMouth.fadeOut(0);
				girlMouthIdle.fadeIn(0);
				inactiveMetalsPlusAcid.fadeOut(500);
			}, 1000);
			
			timeout[1] = setTimeout(function(){
				theFrame.attr("data-done", "true");
				fadeNavsInAuto();
				sendCompletedStatement(1);
			}, 2000);
		}
	});
	
	demoPics.fadeOut(0);
	girlMouth.fadeOut(0);
	boyMouth.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	audioPieces[0] = new AudioPiece("s4-1", 30, 58);
	
	audioPieces[0].addEventListener("ended", function(){
		
	});
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			audioPieces[0].play();
			boyMouth.fadeIn(0);
		}, 1000);
		timeout[1] = setTimeout(function(){
			boyMouth.fadeOut(0);
		}, 5000);
		timeout[2] = setTimeout(function(){
			girlMouthIdle.fadeOut(0);
			girlMouth.fadeIn(0);
		}, 5000);
		timeout[3] = setTimeout(function(){
			activeMetalsPlusAcid.fadeIn(500);
		}, 11000);
		timeout[4] = setTimeout(function(){
			sprite[0].play();
		}, 12000);
		timeout[5] = setTimeout(function(){
			activeMetalsPlusAcid.fadeOut(500);
		}, 22000);
		timeout[6] = setTimeout(function(){
			inactiveMetalsPlusAcid.fadeIn(500);
		}, 22000);
		timeout[7] = setTimeout(function(){
			sprite[1].play();
		}, 23000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(8);
	}, 2000);
}

launch["frame-109"] = function()
{		
		theFrame = $("#frame-109"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boyMouth = $(prefix + ".boy-mouth"),
		vocabulary = $(prefix + ".vocabulary");
	
	boyMouth.fadeOut(0);
	vocabulary.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	audio[0] = new Audio("audio/s5-1.mp3");
	
	startButtonListener = function(){
		timeout[1] = setTimeout(function(){
			boyMouth.fadeIn(0);
			audio[0].play();
			vocabulary.fadeIn(1000);
		}, 1000);
		timeout[2] = setTimeout(function(){
			boyMouth.fadeOut(0);
		}, 4000);
		timeout[3] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}, 16000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(9);
	}, 2000);
}

launch["frame-110"] = function()
{		
		theFrame = $("#frame-110"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		answers = $(prefix + ".answer"),
		keepThinking = $(prefix + ".keep-thinking"),
		instruction = $(prefix + ".instruction"),
		tasks = $(".task"),
		activeTask = 1;
	
	audio[0] = new Audio("audio/correctanswer.mp3");
	audio[1] = new Audio("audio/s6-1.mp3");
	
	tasks.fadeOut(0);
	keepThinking.fadeOut(0);
	instruction.fadeOut(0);
		
	fadeNavsOut();
	fadeLauncherIn();
	
	var answersListener = function(){
		var empty = $(prefix + ".task-" + activeTask + " .empty"),
			tempImage = $(this).css("background-image");
		empty.css("background-image", tempImage);
		
		if($(this).attr("data-correct"))
		{
			empty.css("background-color", "#01A6E8");
			audio[0].play();
			timeout[0] = setTimeout(function(){
				activeTask++;
				tasks.fadeOut(0); 
				$(".task-"+activeTask).fadeIn(0);
				empty = $("#frame-102 .task-" + activeTask + " .empty");
				if(activeTask > 3)
				{
					tasks.fadeOut(500);
					instruction.fadeOut(500);
					theFrame.attr("data-done", "true");
					fadeNavsInAuto();
					sendCompletedStatement(1);
				}
			}, 2000);
		}
		else
		{
			empty.css("background-color", "#FF3C25");
			keepThinking.fadeIn(0);
			timeout[0] = setTimeout(function(){
				keepThinking.fadeOut(0);
				empty.css("background-image", "");
				empty.css("background-color", "");
			}, 2000);
		}
	};
	answers.off("click", answersListener);
	answers.on("click", answersListener);
	
	startButtonListener = function(){
		$(".task-"+activeTask).fadeIn(500);
		instruction.fadeIn(500);
		audio[1].play();
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(10);
	}, 2000);
}

launch["frame-111"] = function()
{		
		theFrame = $("#frame-111"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boyMouth = $(prefix + ".boy-mouth"),
		girlMouth = $(prefix + ".girl-mouth"),
		girlMouthIdle = $(prefix + ".girl-mouth-idle"), 
		carbonates = $(prefix + ".carbonates");
		
	sprite[0] = new Motio(carbonates[0], {
		"fps": "1.5",
		"frames": "11"
	});
	sprite[0].on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	boyMouth.fadeOut(0);
	girlMouth.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	carbonates.fadeOut(0);
	
	audio[0] = new Audio("audio/s7-1.mp3");
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			boyMouth.fadeIn(0);
			audio[0].play();
		}, 1000);
		timeout[1] = setTimeout(function(){
			boyMouth.fadeOut(0);
		}, 4000);
		timeout[2] = setTimeout(function(){
			girlMouth.fadeIn(0);
			girlMouthIdle.fadeOut(0);
		}, 4000);
		timeout[3] = setTimeout(function(){
			carbonates.fadeIn(500);
		}, 5000);
		timeout[4] = setTimeout(function(){
			sprite[0].play();
		}, 6000);
		timeout[4] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}, 14000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(11);
	}, 2000);
}

launch["frame-112"] = function()
{		
		theFrame = $("#frame-112"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		girlMouth = $(prefix + ".girl-mouth"),
		girlMouthIdle = $(prefix + ".girl-mouth-idle"), 
		video = $(prefix + ".video");
	
	video.attr("width", video.parent().css("width"));
	video.attr("height", video.parent().css("height"));
	
	$(window).resize(function(){
		video.attr("width", video.parent().css("width"));
		video.attr("height", video.parent().css("height"));
	});
	
	girlMouth.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	video.fadeOut(0);
	
	audio[0] = new Audio("audio/s9-1.mp3");
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			girlMouthIdle.fadeOut(0);
			girlMouth.fadeIn(0);
			audio[0].play();
		}, 1000);
		timeout[1] = setTimeout(function(){
			video.fadeIn(500);
			video[0].play();
		}, 5000);
		timeout[2] = setTimeout(function(){
			video.fadeOut(500);
			girlMouthIdle.fadeIn(0);
			girlMouth.fadeOut(0);
		}, 28000);
		timeout[3] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(12);
		}, 29000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(12);
	}, 2000);
}

launch["frame-113"] = function()
{		
		theFrame = $("#frame-113"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		draggables = $(prefix + ".ball");
	var draggabillies = [],
		vegetable, basket

	fadeNavsOut();
	fadeLauncherIn();
	
	audio[0] = new Audio("audio/s10-1.mp3");
	audio[1] = new Audio("audio/correctanswer.mp3");

	
	for (var i = 0; i < draggables.length; i ++)
		draggabillies[i] = new Draggabilly(draggables[i]);
	
	var onStart = function(instance, event, pointer)
	{
		vegetable = $(event.target);
	};
	var onEnd= function(instance, event, pointer)
	{
		vegetable.fadeOut(0);
		basket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		
		if(vegetable.attr("data-key") === basket.attr("data-key"))
		{	
			audio[1].play();
			basket.html(vegetable.html());
			basket.css("background-color", "#4EC7DD");
			basket.css("text-align", "center");
			vegetable.remove();
			if(!$(prefix + ".ball").length)
			{
				theFrame.attr("data-done", "true");
				fadeNavsInAuto();
				sendCompletedStatement(1);
			}
		}	
		else
		{
			vegetable.fadeIn(0);
			vegetable.css("left", "");
			vegetable.css("top", "");
		}	
	};
	
	for (var i = 0; i < draggabillies.length; i ++)
	{
		draggabillies[i].on("dragStart", onStart);
		draggabillies[i].on("dragEnd", onEnd);
	}
	
	startButtonListener = function(){
		audio[0].play();
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(13);
	}, 2000);
}

launch["frame-114"] = function()
{		
		theFrame = $("#frame-114"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		mouths = $(prefix + ".mouths");
	
	mouths.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	audio[0] = new Audio("audio/s11-1.mp3");
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			mouths.fadeIn(0);
			audio[0].play();
		}, 1000);
		timeout[1] = setTimeout(function(){
			mouths.fadeOut(0);
		}, 4000);
		timeout[2] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}, 6000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(14);
	}, 2000);
}

launch["frame-301"] = function()
{
		theFrame = $("#frame-301"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		nextButton = $(prefix + ".next-button"),
		parts = $(prefix + ".part"),
		answer = $(prefix + ".answer"), 
		result = $(prefix + ".label"), 
		taskButton = $(prefix + ".task-button"), 
		homeButton = $(prefix + ".home-button"), 
		correctNum = 0,
		taskScreen = $(prefix + ".task");
		activeTask = 0;
		
	taskScreen.fadeOut(0);
	taskScreen.filter(".task-" + activeTask).fadeIn(0);
	
	resultLabel = result.html();
	
	parts.fadeOut(0);
	// $("part-1").fadeIn(0);
	
	var answerListener = function(){
		
		var currAnswer = $(this);
		
		currAnswer.off("click", answerListener);
		currAnswer.siblings().off("click", answerListener);
		
		if(currAnswer.attr("data-correct"))
		{
			correctNum ++;
			currAnswer.css("background-color","green");
		}
		else
		{
			currAnswer.css("background-color","red");
			var trueanswer = currAnswer.siblings().filter(".true");
			trueanswer.css("background-color","green");
		}
			
		result.html(resultLabel + " " + correctNum);
		++activeTask;
		
		timeout[0] = setTimeout(function(){
			taskScreen.fadeOut(0);
			$(prefix + ".task-" + activeTask).fadeIn(0);
		}, 3000);
	};
	
	answer.on("click", answerListener);

	taskButton.on("click", function () {
		taskScreen.filter(".task-0").fadeOut(0);
		activeTask++;
		taskScreen.filter(".task-1").fadeIn(0);
	});

}

var hideEverythingBut = function(elem)
{
	if(theFrame)
		theFrame.html(theClone.html());
	
	var frames = $(".frame");
	frames.fadeOut(0);
	elem.fadeIn(0);
	regBox = $(".reg-box");
	fadeTimerOut();
	
	console.log(audio.length);
	
	if(!globalName)
		regBox.fadeOut(0);
	
	if(elem.attr("id") === "frame-000")
	{
		fadeNavsOut();
		fadeTimerOut();
	}
	
	for(var i = 0; i < audio.length; i ++)
		audio[i].pause();
	
	for(var i = 0; i < audioPieces.length; i ++)
		audioPieces[i].pause();
	
	for(var i = 0; i < timeout.length; i++)
		clearTimeout(timeout[0]);
	
	if(elem.attr("id") === "frame-000")
		regBox.fadeIn(0);
	
	launch[elem.attr("id")]();
	initMenuButtons();
}

var initMenuButtons = function(){
	$(".link").on("click", function(){
		var elem = $("#"+$(this).attr("data-link"));
		hideEverythingBut(elem);
	});
};

var showModel = function(content){
	model.fadeIn(1000);
	modelContent.html(content);
};

var hideModel = function(){
	model.fadeOut(1000);
};

var showMap = function()
{
	
}
var main = function()
{
	var video = $(".intro-video"),
		pic = $(".intro-pic");
	
	hideEverythingBut($("#frame-000"));
	
	setMenuStuff();
	video.attr("width", video.parent().css("width"));
	video.attr("height", video.parent().css("height"));
	video[0].play();
	
	timeout[0] = setTimeout(function(){
		video.hide();
	}, 10000);
	timeout[1] = setTimeout(function(){
		pic.hide(); 
	}, 15000);
};

$(document).ready(main);